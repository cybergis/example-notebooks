# Example Jupyter Notebooks

This is a list of example Jupyter notebooks created by any person. This preferentially lists notebooks relevant for geospatial operations, but may include other supporting examples.

#### What is this repository for?

This will serve as a reference for other notebooks that may have useful examples, or have a style/structure worth emulating.

#### Contribution guidelines

Anyone may suggest contributions. CyberGIS Center personnel may directly edit this list.

# Notebook List

#### R Training Notebooks
https://bitbucket.org/cybergis/r-training-notebooks  
Produced by CyberGIS Center. Lab 3 is a parallel zonal statistics workflow.  

#### Geospatial Data with Open Source Tools in Python
https://github.com/kjordahl/SciPy-Tutorial-2015  
Created by Kelsey Jordahl for SciPy 2015  
Cloned here: https://bitbucket.org/cybergis/geospatial-python  

#### Python Demos
https://bitbucket.org/cybergis/python-demos  
Project to create set of short examples of geospatial operations in Python, started by Yassaman. Currently includes raster statistics and histograms.